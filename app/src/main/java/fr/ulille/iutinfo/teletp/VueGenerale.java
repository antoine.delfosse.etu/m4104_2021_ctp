package fr.ulille.iutinfo.teletp;

import android.app.Application;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;

public class VueGenerale extends Fragment {

    String salle;
    String poste;
    SuiviViewModel model;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.vue_generale, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        final String DISTANCIEL = getResources().getStringArray(R.array.list_salles)[0];
        poste = "";
        salle = DISTANCIEL;
        model = new ViewModelProvider(requireActivity()).get(SuiviViewModel.class);
        Spinner spinnerSalle = (Spinner) view.findViewById(R.id.spSalle);
        ArrayAdapter<CharSequence> adapterSalle = ArrayAdapter.createFromResource(this.getContext(), R.array.list_salles, R.layout.support_simple_spinner_dropdown_item);
        adapterSalle.setDropDownViewResource((R.layout.support_simple_spinner_dropdown_item));
        spinnerSalle.setAdapter(adapterSalle);
        update();

        Spinner spinnerPoste = (Spinner) view.findViewById(R.id.spPoste);
        ArrayAdapter<CharSequence> adapterPoste = ArrayAdapter.createFromResource(this.getContext(), R.array.list_postes, R.layout.support_simple_spinner_dropdown_item);
        adapterPoste.setDropDownViewResource((R.layout.support_simple_spinner_dropdown_item));
        spinnerPoste.setAdapter(adapterSalle);
        update();


        view.findViewById(R.id.btnToListe).setOnClickListener(view1 -> {
            EditText name = getActivity().findViewById(R.id.tvLogin);
            model.setUsername(name.getText().toString());
            NavHostFragment.findNavController(VueGenerale.this).navigate(R.id.generale_to_liste);
        });

        update();
        // TODO Q9
    }

    public void update() {
        Spinner spPoste = (Spinner) getView().findViewById(R.id.spPoste);
        Spinner spSalle = (Spinner) getView().findViewById(R.id.spSalle);
        if(salle.equals("Distanciel")) {
            spPoste.setVisibility(View.INVISIBLE);
            spPoste.setEnabled(false);
            model.setLocalisation("Distanciel");
        } else {
            spPoste.setVisibility(View.VISIBLE);
            spPoste.setEnabled(true);
            model.setLocalisation(""+spSalle.getSelectedItem() + " : " + spPoste.getSelectedItem());
        }
    }
    // TODO Q9
}